export default class GotService {
	constructor() {
		this._apiBase = 'https://www.anapioficeandfire.com/api';
	}

	async getResource(url) {
		const response = await fetch(`${this._apiBase}${url}`);

		if (!response.ok) {
			throw new Error(`Could not fetch ${url}, status ${response.status}`)
		}
		return await response.json();

	}

	getAllCharacters = async () => {
		const allCharacters = await this.getResource('/characters?page=5&pageSize=10');
		return allCharacters.map(this._transformCharacters);
	}
	getCharacter = async (id) => {
		const character = await this.getResource(`/characters/${id}`);
		return this._transformCharacters(character);
	}

	getAllBooks = async () => {
		const allBooks = await this.getResource('/books');
		return allBooks.map(this._transformBook);
	}
	getBook = async (id) => {
		const book = await this.getResource(`/books/${id}`);
		return this._transformBook(book);
	}

	getAllHouses = async () => {
		const allHouse = await this.getResource('/houses');
		return allHouse.map(this._transformHouse);
	}
	getHouse = async (id) => {
		const house = await this.getResource(`/houses/${id}`);
		return this._transformHouse(house);
	}

	isSet(data) {
		if (data) {
			return data
		} else {
			return 'no data :('
		}
	}

	_extractId = (item) => {
		const idRegExp = /\/([0-9]*)$/;
		return item.url.match(idRegExp)[1];
	}

	_transformCharacters = (char) => {
		return {
			name: this.isSet(char.name),
			gender: this.isSet(char.gender),
			born: this.isSet(char.born),
			died: this.isSet(char.died),
			culture: this.isSet(char.culture),
			id: this._extractId(char)
		}
	}

	_transformHouse = (house) => {
		return {
			name: this.isSet(house.name),
			region: this.isSet(house.region),
			words: this.isSet(house.words),
			titles: this.isSet(house.titles),
			overlords: this.isSet(house.overlords),
			ancestralWeapons: this.isSet(house.ancestralWeapons),
			id: this._extractId(house)
		}
	}

	_transformBook = (book) => {
		return {
			name: this.isSet(book.name),
			numberOfPages: this.isSet(book.numberOfPages),
			publisher: this.isSet(book.publisher),
			released: this.isSet(book.released),
			id: this._extractId(book)
		}
	}
}