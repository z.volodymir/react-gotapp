import React from 'react';

import img from './warning.jpg'

function ErrorMessage() {
	return (
		<>
			<img className="w-25 mr-3" src={img} alt={img}/>
			<span className="text-uppercase font-weight-bold text-danger">Something goes wrong!</span>
		</>
	)
}
export default ErrorMessage;