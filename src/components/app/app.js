import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Col, Row, Container} from 'reactstrap';
import Header from '../header';
import RandomChar from '../randomChar';
import CharactersPage from "../pages/charactersPage";
import HousesPage from "../pages/housesPage";
import BooksPage from "../pages/booksPage";
import BooksItem from "../pages/booksItem";

export default class App extends React.Component {

    state = {
        showRandomChar: true,
    }

    toggleRandomChar = () => {
            this.setState((state) => {
                return {
                    showRandomChar: !state.showRandomChar
                }
            })
    }

    render() {
        const {showRandomChar} = this.state;

        const randomChar = showRandomChar ? <RandomChar/> : null;

        return (
            <Router>
                <>
                    <Container>
                        <Header/>
                    </Container>
                    <Container>
                        <Row>
                            <Col lg={{size: 5, offset: 0}}>
                                {randomChar}
                                <button
                                    onClick={this.toggleRandomChar}
                                    className="btn btn-primary mb-5"
                                >Toggle random characters
                                </button>
                            </Col>
                        </Row>

                        <Route path="/characters" component={CharactersPage}/>
                        <Route path="/houses" component={HousesPage}/>
                        <Route path="/books" exact component={BooksPage}/>
                        <Route path="/books/:id" render={
                            ({match}) => {
                                const {id} = match.params;
                                return <BooksItem bookId={id}/>
                        }}/>

                    </Container>
                </>
            </Router>
        )
    }
}