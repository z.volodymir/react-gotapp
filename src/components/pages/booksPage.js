import React from 'react';
import ItemList from "../itemList";
import ErrorMessage from "../errorMessage/errorMessage";
import GotService from "../../services/gotService";
import {withRouter} from 'react-router-dom';

// import ItemDetails, {Field} from "../itemDetails";
// import RowBlock from "../rowBlock/rowBlock";

class BooksPage extends React.Component {

	gotService = new GotService();

	state = {
		// itemId: null,
		error: false
	}

	// onItemSelected = (id) => {
	// 	this.setState({
	// 		itemId: id
	// 	});
	// }

	componentDidCatch() {
		this.setState({
			error: true
		});
	}

	render() {

		if (this.state.error) {
			return <ErrorMessage/>
		}

		// const itemList = (
		// 	<ItemList
		// 		onItemSelected={this.onItemSelected}
		// 		getData={this.gotService.getAllBooks}
		// 		renderItem={({name, numberOfPages}) => `${name} (${numberOfPages})`}
		// 	/>
		// );
		//
		// const itemDetails = (
		// 	<ItemDetails
		// 		itemId={this.state.itemId}
		// 		getData={this.gotService.getBook}
		// 	>
		// 		<Field field="numberOfPages" label="Number of pages"/>
		// 		<Field field="publisher" label="Publisher"/>
		// 		<Field field="released" label="Released"/>
		// 	</ItemDetails>
		// )

		return (
			// <RowBlock left={itemList} right={itemDetails}/>
				<ItemList
					onItemSelected={(itemId) => {
						this.props.history.push(itemId);
					}}
					getData={this.gotService.getAllBooks}
					renderItem={({name, numberOfPages}) => `${name} (${numberOfPages})`}
				/>
		)
	}
}

export default withRouter(BooksPage);