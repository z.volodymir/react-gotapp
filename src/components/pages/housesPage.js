import React from 'react';
import ItemList from "../itemList";
import ItemDetails, {Field} from "../itemDetails";
import ErrorMessage from "../errorMessage/errorMessage";
import GotService from "../../services/gotService";
import RowBlock from "../rowBlock/rowBlock";

export default class HousesPage extends React.Component {

	gotService = new GotService();

	state = {
		itemId: null,
		error: false
	}

	onItemSelected = (id) => {
		this.setState({
			itemId: id
		});
	}

	componentDidCatch() {
		this.setState({
			error: true
		});
	}

	render() {

		if (this.state.error) {
			return <ErrorMessage/>
		}

		const itemList = (
			<ItemList
				onItemSelected={this.onItemSelected}
				getData={this.gotService.getAllHouses}
				renderItem={({name, region}) => `${name} (${region})`}
			/>
		);

		const itemDetails = (
			<ItemDetails
				itemId={this.state.itemId}
				getData={this.gotService.getHouse}
			>
				<Field field="region" label="Region"/>
				<Field field="words" label="Words"/>
				<Field field="titles" label="Titles"/>
				<Field field="overlords" label="Overlords"/>
				<Field field="ancestralWeapons" label="AncestralWeapons"/>
			</ItemDetails>
		)

		return (
			<RowBlock left={itemList} right={itemDetails}/>
		)
	}
}